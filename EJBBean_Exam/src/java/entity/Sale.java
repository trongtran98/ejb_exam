/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TrongTran
 */
@Entity
@Table(name = "Sale")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sale.findAll", query = "SELECT s FROM Sale s")
    , @NamedQuery(name = "Sale.findBySlNo", query = "SELECT s FROM Sale s WHERE s.slNo = :slNo")
    , @NamedQuery(name = "Sale.findBySalesmanID", query = "SELECT s FROM Sale s WHERE s.salesmanID = :salesmanID")
    , @NamedQuery(name = "Sale.findBySalesmanNameName", query = "SELECT s FROM Sale s WHERE s.salesmanNameName = :salesmanNameName")
    , @NamedQuery(name = "Sale.findByDos", query = "SELECT s FROM Sale s WHERE s.dos = :dos")})
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SlNo")
    private String slNo;
    @Size(max = 50)
    @Column(name = "SalesmanID")
    private String salesmanID;
    @Size(max = 250)
    @Column(name = "SalesmanNameName")
    private String salesmanNameName;
    @Size(max = 250)
    @Column(name = "DOS")
    private String dos;
    @JoinColumn(name = "ProdID", referencedColumnName = "ProdID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Product prodID;

    public Sale() {
    }

    public Sale(String slNo) {
        this.slNo = slNo;
    }

    public String getSlNo() {
        return slNo;
    }

    public void setSlNo(String slNo) {
        this.slNo = slNo;
    }

    public String getSalesmanID() {
        return salesmanID;
    }

    public void setSalesmanID(String salesmanID) {
        this.salesmanID = salesmanID;
    }

    public String getSalesmanNameName() {
        return salesmanNameName;
    }

    public void setSalesmanNameName(String salesmanNameName) {
        this.salesmanNameName = salesmanNameName;
    }

    public String getDos() {
        return dos;
    }

    public void setDos(String dos) {
        this.dos = dos;
    }

    public Product getProdID() {
        return prodID;
    }

    public void setProdID(Product prodID) {
        this.prodID = prodID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (slNo != null ? slNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sale)) {
            return false;
        }
        Sale other = (Sale) object;
        if ((this.slNo == null && other.slNo != null) || (this.slNo != null && !this.slNo.equals(other.slNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Sale[ slNo=" + slNo + " ]";
    }
    
}
