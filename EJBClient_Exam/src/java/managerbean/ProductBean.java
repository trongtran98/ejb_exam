/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managerbean;

import entity.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import sessionbeanlocal.ProductFacadeLocal;

/**
 *
 * @author TrongTran
 */

public class ProductBean {
    @EJB
    ProductFacadeLocal productFacade; 
    /**
     * Creates a new instance of ProductBean
     */
    public ProductBean() {
    }
    
    private List<Product> listProduct;
    
    public List<Product> getListProduct() {
        listProduct = productFacade.findAll();
        return listProduct;
    }

    public void setListProduct(List<Product> listProduct) {
        this.listProduct = listProduct;
    }
    
}
