/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managerbean;

import entity.Sale;
import java.util.List;
import javax.ejb.EJB;
import sessionbeanlocal.ProductFacadeLocal;
import sessionbeanlocal.SaleFacadeLocal;

/**
 *
 * @author TrongTran
 */
public class SaleBean {

    @EJB
    SaleFacadeLocal saleFacade;

    /**
     * Creates a new instance of SaleBean
     */
    public SaleBean() {
    }
    private List<Sale> listSale;

    public List<Sale> getListSale() {
        listSale = saleFacade.findAll();
        return listSale;
    }

    public void setListSale(List<Sale> listSale) {
        this.listSale = listSale;
    }
    
}
